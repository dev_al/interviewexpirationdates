﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview
{
	public class ExpirationDates
	{
		public DateTime ExpirateDate { get; set; }
		public int ProductId { get; set; }

		public int WareHouseId { get; set; }
		public int Quantity { get; set; }

	}

	public class InvoiceProduct
	{
		public int ProductId { get; set; }
		public int WarehouseId { get; set; }
		public int Quantity { get; set; }
		public DateTime ExpirationDate { get; set; }
	}
}
