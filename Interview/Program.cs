﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Interview
{
	class Program
	{
		static void Main(string[] args)
		{
			//Expiration Dates
			var expirationDates = new List<ExpirationDates>()
			{
				new ExpirationDates(){Quantity = 2, ProductId = 1, WareHouseId = 1, ExpirateDate = new DateTime(2021, 7, 22)},
				new ExpirationDates(){Quantity = 7, ProductId = 2, WareHouseId = 1, ExpirateDate = new DateTime(2021, 8, 2)},
				new ExpirationDates(){Quantity = 3, ProductId = 1, WareHouseId = 1, ExpirateDate = new DateTime(2021, 7, 23)},
				new ExpirationDates(){Quantity = 9, ProductId = 2, WareHouseId = 2, ExpirateDate = new DateTime(2021, 8, 12)},
				new ExpirationDates(){Quantity = 1, ProductId = 2, WareHouseId = 2, ExpirateDate = new DateTime(2021, 9, 7)},
				new ExpirationDates(){Quantity = 1, ProductId = 2, WareHouseId = 1, ExpirateDate = new DateTime(2021, 12, 12)},
				new ExpirationDates(){Quantity = 10, ProductId = 1, WareHouseId = 2, ExpirateDate = new DateTime(2021, 7, 22)},
				new ExpirationDates(){Quantity = 14, ProductId = 1, WareHouseId = 1, ExpirateDate = new DateTime(2021, 7, 27)},
			};

			var product = new InvoiceProduct()
			{
				Quantity =  7,
				ProductId = 1,
				WarehouseId = 1,
				ExpirationDate = new DateTime(2021, 7, 22)

			};

			SellProduct(product, expirationDates);

			foreach (var expirationDate in expirationDates)
			{
				Console.WriteLine(" Data "+ expirationDate.ExpirateDate + " Sasia"+ expirationDate.Quantity);
			}

		}


		public static void SellProduct(InvoiceProduct product, List<ExpirationDates> expirationDates)
		{
		}


	}
}
