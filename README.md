# README #

DEV Interview - ExpirationDates

### Kerkesat
Te zhvillohet metoda e cila do te mundesoje zbritjen e sasive te produkteve nga magazina ne baze te dates se skadences qe eshte zgjedhur.
Ne shitje te produktit do te spcifikohet data e skadences dhe sasia qe po shitet. 
Fillimisht nga magazina do te zbritet sasia qe i perket dates qe eshte specifikuar. Nese sasia qe po shitet eshte me e madhe se sasia qe ndodhet ne magazine per ate date skadence, sasia duhet te zbritet nga data e radhes me e afert.
Shembull: 
produkti 'paracetamol' do te shitet me sasi 5 dhe date skadence 10 Qershor 2021. Produkti do te shitet nga magazina 1. 

Ne magazinen 1 per produktin 'paracetamol' kemi 
3 njesi te cilat skadojne ne daten 10 Qershor 2021
6 njesi te cilat skadojne ne daten 12 tetor 2021
3 njesi te cilat skadojne ne daten 11 Qershor 2021

Duhet qe fillimisht te zbriten njesite nga data 10 Qershor qe eshte data e zgjedhur
Me pas duhet te zbriten nga data 11 Qershor qe eshte data me e afert 

Nese gjendja ne magazine per nje date te caktuar shkon ne 0, rekordi te fshihet. 